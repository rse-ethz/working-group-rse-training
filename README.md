# Working Group RSE Training

This working groups is about training / education of RSEs and has no defined end.


## Ideas

- check what other countries are doing:
    - https://us-rse.org/wg/education_training/
    - Contact Jeremy Cohen (jhc02@doc.ic.ac.uk) (byte sized RSE)
    - Contact authors from https://arxiv.org/pdf/2311.11457.pdf (they work on RSE training ideas)
- sync with events working group to offer trainings / workshops
- collect, review, structure and publish training material for different levels and research domains.
- low entry trainings / meetups to attract members.